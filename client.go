package main

import (
	"log"
	"net"
)


func main() {
	log.Println("Start Client Success")

	runUdpClient()
}

func runUdpClient() {
	log.Println("Run Udp Client Success.")

	raddr, err := net.ResolveUDPAddr("udp", "127.0.0.1:8089")

	if nil != err {
		log.Fatal(err)
	}

	conn, err := net.DialUDP("udp", nil, raddr)

	log.Println(conn.RemoteAddr())

	if nil != err {
		log.Fatal(err)
	}

	_, err = conn.Write([]byte("11111111111111111111111111111111111111111111"))

	if nil != err {
		log.Fatalln(err)
	}
}