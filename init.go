package main

import (
	"log"
	"os"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Llongfile)
}

func setLogOutput() {
	f, err := os.OpenFile("./log.log", os.O_APPEND | os.O_CREATE, 0755)

	if nil != err {
		log.Fatalln(err)
	}

	log.SetOutput(f)
}