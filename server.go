package main

import (
	"bytes"
	"encoding/json"
	"log"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"sync"
	"time"
)

func main() {
	log.Println("Start Server Success.")

	// run by goroutine model
	go runUdpServer()

	runHttpServer()

	time.Sleep(1 * time.Second)
}


// 启动一个http服务
func runHttpServer() {
	log.Println("Run Http Server Success.")

	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		// 获取子域名,eg: 239582.wejuji.cn
		//subDomain := request.Host[:len(request.Host) - len(MainDomain) - 1]

		// 查询绑定列表
		udpAddr, ok := domainUdpMap[request.Host]

		if !ok {
			log.Println("域名没有存在绑定关系")
			return
		}

		head, err := json.Marshal(request.Header)

		if nil != err {
			log.Println(err)
			// 错误处理
		}

		headerLen := int16(len(head))

		buf := bytes.Buffer{}
		buf.Read([]byte{byte(headerLen >> 8), byte(headerLen)})
		buf.Read(head)
		buf.ReadFrom(request.Body)

		var i = 0
		var hasEnd uint8 = 0
		var pa = make([]byte, 1024)

		for  {
			i++

			s := buf.Next(1024 -5)
			n := copy(pa[6:], s)

			if n == 0 { // 没有copy到数据表示已经读取完成了
				hasEnd = 1
			}

			makePackageHeader(pa, i, hasEnd)

			// 转发消息，最大允许同时转发300条请求
			if _, err := udpConn.WriteToUDP(pa, udpAddr); nil != err {
				log.Println(err)
				return
			}

			if 1 == hasEnd {
				break
			}
		}

	})

	log.Fatal(http.ListenAndServe(":8088", nil))
}

func makePackageHeader(p []byte, i int, hasEnd uint8) {
	p[0] = byte(i >> 24)
	p[1] = byte(i >> 16)
	p[2] = byte(i >> 8)
	p[3] = byte(i)
	p[4] = hasEnd // 一个字节表示是否最后一个包，有点奢侈
}


type msg struct {
	Header http.Header
	Body []byte
}



// 全局udp连接对象
var udpConn *net.UDPConn

// 默认客户端连接池
var defaultClientPools clientPools

type msgPool struct {
	n int
	msg []byte
	addr *net.UDPAddr
}

var clientMsgPool chan *msgPool

// 允许的最大缓存量
const MaxClientMsgPool = 200

const MainDomain = "wejuji.cn"


// 接受一个client数据包，包含当前客户端IP，是否手动绑定域名，
func runUdpServer() {
	log.Println("Run Udp Server Success.")

	laddr, err := net.ResolveUDPAddr("udp", ":8089")

	if nil != err {
		log.Fatalln(err)
	}

	// 监听udp链接
	udpConn, err = net.ListenUDP("udp", laddr)

	if nil != err {
		log.Fatal(err)
	}

	defer udpConn.Close()

	// 初始化连接池
	defaultClientPools = newClientPools()

	// 初始化客户端消息池
	clientMsgPool = make(chan *msgPool, MaxClientMsgPool)

	log.Println("Server Listen: ", udpConn.LocalAddr())

	b := make([]byte, 1024)

	go handler()

	for  {
		n, clientAddr, err := udpConn.ReadFromUDP(b)

		if nil != err {
			log.Println(err)
		}

		clientMsgPool <- &msgPool{
			n,
			b,
			clientAddr,
		}
	}
}

// 处理客户端上报的信息
func handler() {
	msgPool := <- clientMsgPool

	log.Println(msgPool)

	msg := msgPool.parseMsg()

	// 根据type分配处理
	switch msg.Type {
	case msgType_HANDLE:
		var domain string

		if msg.Content == nil { // 不带domain的报文处理
			// 不带domain的需要分配一个domain
			domain =  subDomain() + "." + MainDomain
		} else { // 带domain的报文处理
			domain = string(msg.Content)
		}

		// 绑定domain到客户端ip
		defaultClientPools.bindDomain(domain, msgPool.addr)
	case msgType_HTTP:

	default:
		// 类型错误
	}
}



var mutex = sync.Mutex{}

// 生成一个随机的子域名
func subDomain() string {
	mutex.Lock()
	defer mutex.Unlock()

	rand.Seed(time.Now().Unix())
	return "tq" + strconv.FormatInt(int64(rand.Int31()), 10)
}


type msgType uint8

const (
	msgType_HANDLE msgType = iota
	msgType_HTTP
)

type msgPackage struct {
	Type msgType // 1：状态上报，2：http数据响应
	Content []byte
}

// 解析客户端数据包
func (m *msgPool) parseMsg() *msgPackage {
	var content []byte

	if m.n - 1 != 0 {
		content = m.msg[1:m.n - 1]
	}

	return &msgPackage{
		msgType(m.msg[0]),
		content,
	}
}


// 客户端连接池
type clientPools map[*net.UDPAddr]string

var domainUdpMap = make(map[string]*net.UDPAddr)

// 绑定域名
func (c clientPools) bindDomain(domain string, addr *net.UDPAddr) {
	log.Printf("Domain Bind: %s -> %v", domain, addr)

	c[addr] = domain

	//TODO: 查看是否已经存在相同域名的地址绑定, 修改为最新的
	addr, ok := domainUdpMap[domain]
	if ok {
		delete(defaultClientPools, addr)
	}

	domainUdpMap[domain] = addr
}

// 获取一个新的连接池
func newClientPools() clientPools {
	return make(clientPools)
}